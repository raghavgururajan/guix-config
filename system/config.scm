(use-modules
 (gnu)
 (gnu system nss)
 (gnu system privilege)
 (gnu system setuid)
 (gnu system shadow)
 (rg packages suckless)
 (rg services base))
(use-package-modules
 android
 aspell
 certs
 disk
 fonts
 fontutils
 freedesktop
 glib
 gnome
 gstreamer
 hunspell
 kde-frameworks
 linux
 lisp
 package-management
 polkit
 qt
 wm
 xorg
 xdisorg)
(use-service-modules
 authentication
 avahi
 certbot
 cups
 dbus
 desktop
 dns
 linux
 networking
 nix
 pm
 security-token
 sound
 virtualization
 vpn
 xorg)

(operating-system
 ;; Use LTS versions of the kernel.
 (kernel linux-libre-lts)
 (kernel-arguments
  (append
   (list
    ;; Enable fan control from userspace.
    "thinkpad_acpi.fan_control=1")
   %default-kernel-arguments))
 #;(initrd-modules
  ;; Remove incompatible modules.
  (delete "simplefb" %base-initrd-modules))
 (keyboard-layout
  (keyboard-layout "us"))
 (bootloader
  (bootloader-configuration
   (bootloader
    ;; Don't install Grub binaries on disk,
    ;; but still generate Grub configuration.
    (bootloader
     (inherit grub-bootloader)
     (installer #~(const #t))))
   (keyboard-layout keyboard-layout)))
 (label "x200t")
 (host-name "x200t")
 (mapped-devices
  (append
   ;; LUKS
   (list
    (mapped-device
     (source
      (uuid "7a0cbb90-7f10-4db9-a5cb-0c923f8e560a"))
     (targets
      (list
       "x200t"))
     (type luks-device-mapping)))
   ;; LVM
   (list
    (mapped-device
     (source "x200t")
     (targets
      (list
       "x200t-root"
       "x200t-swap"))
     (type lvm-device-mapping)))))
 (file-systems
  (append
   (list
    (file-system
     (type "btrfs")
     (mount-point "/")
     (device (file-system-label "x200t_root"))
     (flags '(no-atime))
     (options "space_cache=v2")
     (needed-for-boot? #t)
     (dependencies mapped-devices)))
   %base-file-systems))
 (swap-devices
  (list
   (swap-space
    (target (file-system-label "x200t_swap"))
    (dependencies mapped-devices))))
 (users
  (append
   (list
    (user-account
     (name "rg")
     (comment "Raghav Gururajan")
     (group "users")
     (supplementary-groups
      '("adbusers" "audio" "cdrom" "kvm" "libvirt" "lp"
        "netdev" "tape" "tor" "video" "wheel"))))
   %base-user-accounts))
 (packages
  (append
   ;; Dictionaries
   (list
    aspell-dict-en
    hunspell-dict-en
    hunspell-dict-en-ca)
   ;; Fonts
   (list
    font-gnu-freefont
    font-gnu-unifont
    `(,font-gnu-unifont"pcf")
    `(,font-gnu-unifont"psf")
    font-google-noto
    `(,font-google-noto"ttf"))
   ;; Icons
   (list
    adwaita-icon-theme
    breeze-icons
    hicolor-icon-theme
    oxygen-icons)
   ;; Languages
   (list
    sbcl)
   ;; Modules
   (list
    sbcl-stumpwm-ttf-fonts)
   ;; Plugins
   (list
    gst-libav
    gst-plugins-base
    gst-plugins-bad
    gst-plugins-good
    gst-plugins-ugly)
   ;; Programs
   (list
    dbus
    desec-certbot-hook
    flatpak
    network-manager-applet
    nix
    st-custom
    stumpwm
    xinit)
   %base-packages))
 (timezone "Asia/Kolkata")
 (locale "en_IN.UTF-8")
 (name-service-switch %mdns-host-lookup-nss)
 (services
  (append
   ;; Device
   (list
    (service cups-service-type
             (cups-configuration
              (web-interface? #t)))
    (service inputattach-service-type
             (inputattach-configuration
              (device-type "wacom")
              (device "/dev/ttyS4")
              (baud-rate 38400)))
    (service sane-service-type)
    (service udisks-service-type
             (udisks-configuration))
    (udev-rules-service 'android android-udev-rules
                        #:groups '("adbusers")))
   ;; Display
   (list
    (service colord-service-type)
    (service xorg-server-service-type
             (xorg-configuration
              (modules
               ;; Load these driver modules only.
               (list
                xf86-input-libinput
                xf86-video-intel))
              (drivers
               ;; Use intel specific video driver.
               (list
                "intel"))
              (keyboard-layout keyboard-layout))))
   ;; Memory
   (list
    (service earlyoom-service-type))
   ;; Network
   (list
    (service avahi-service-type)
    (service bitmask-service-type)
    (service bluetooth-service-type
             (bluetooth-configuration
              (auto-enable? #t)))
    (service geoclue-service-type
             (geoclue-configuration
              (geoclue geoclue)
              (whitelist '())
              (wifi-geolocation-url "https://location.services.mozilla.com/v1/geolocate?key=geoclue")
              (submit-data? #f)
              (wifi-submission-url "https://location.services.mozilla.com/v1/submit?key=geoclue")
              (submission-nick "geoclue")
              (applications
               (append
                (list
                 (geoclue-application "redshift" #:system? #f))
                %standard-geoclue-applications))))
    (service modem-manager-service-type)
    (service network-manager-service-type)
    (service ntp-service-type)
    (service tor-service-type)
    (service usb-modeswitch-service-type)
    (service wireguard-service-type
             (wireguard-configuration
              (interface "mullvad") ;; Device: Normal Gopher
              (addresses '("10.67.51.110/32" "fc00:bbbb:bbbb:bb01::4:336d/128"))
              (dns '("10.64.0.1"))
              (peers
               (list
                (wireguard-peer
                 (name "gb-lon-wg-008")
                 (endpoint "141.98.252.138:51820")
                 (public-key "uHkxYjfx6yzPHSdyqYqSEHsgFNFV8QCSV6aghuQK3AA=")
                 (allowed-ips '("0.0.0.0/0" "::0/0"))
                 (keep-alive "25"))))
                (post-up '("iptables -I OUTPUT ! -o %i -m mark ! --mark $(wg show %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT && ip6tables -I OUTPUT ! -o %i -m mark ! --mark $(wg show %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT"))
                (pre-down '("iptables -D OUTPUT ! -o %i -m mark ! --mark $(wg show %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT && ip6tables -D OUTPUT ! -o %i -m mark ! --mark $(wg show %i fwmark) -m addrtype ! --dst-type LOCAL -j REJECT"))))
    (service wpa-supplicant-service-type))
   ;; Operation
   (list
    (service nix-service-type))
   ;; Power
   (list
    (service thermald-service-type
             (thermald-configuration
              (ignore-cpuid-check? #t)))
    (service tlp-service-type)
    (service upower-service-type))
   ;; Security
   (list
    (service accountsservice-service-type)
    (service certbot-service-type
             (certbot-configuration
              (email "admin@raghavgururajan.name")
              (certificates
               (list
                (certificate-configuration
                 (name "nearlyfreespeech")
                 (domains '("www.raghavgururajan.name"))
                 (challenge "dns")
                 (authentication-hook "/run/current-system/profile/etc/desec/hook.sh")
                 (cleanup-hook "/run/current-system/profile/etc/desec/hook.sh"))))))
    (service elogind-service-type)
    ;(service fprintd-service-type)
    (service polkit-service-type)
    (service pcscd-service-type))
   ;; Sound
   (list
    (service alsa-service-type)
    (service pulseaudio-service-type))
   ;; Virtualization
   (list
    (service libvirt-service-type)
    (service qemu-binfmt-service-type
             (qemu-binfmt-configuration
              (platforms
               (lookup-qemu-platforms "aarch64"))))
    (service virtlog-service-type))
   (modify-services %base-services
		    ;; Automatically login at startup.
		    #;(mingetty-service-type config =>
					   (auto-login-to-tty
					    config "tty2" "rg"))
		    (guix-service-type config =>
				       (guix-configuration
					(inherit config)
					(substitute-urls
					 (append
					  (list
                                           ;;"https://bags.whereis.xn--q9jyb4c"
					   "https://substitutes.nonguix.org")
					  %default-substitute-urls))
					(authorized-keys
					 (append
					  (list
					   (plain-file "0cool.pub" "(public-key (ecc (curve Ed25519) (q #7C89960E4D705E5524F69126D567B9BC5EF3F58D3220F36029954B7ED0164AC5#)))")					   (plain-file "bags.pub" "(public-key (ecc (curve Ed25519) (q #C75E88236DB6B935F9D63B30872C70AB26328AACCC7FB6A72102C3073D1DB984#)))")
					   (plain-file "nonguix.pub" "(public-key (ecc (curve Ed25519) (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)))"))
					  %default-authorized-guix-keys)))))))
 (privileged-programs
  (append
   (list
    (privileged-program
     (program (file-append util-linux "/sbin/losetup"))
     (setuid? #t)))
   %default-privileged-programs)))


# Guix Configuration

Custom declarations for [Guix](https://guix.gnu.org).

## Usage

Clone this repository.

``` shell
git clone https://git.sr.ht/~raghavgururajan/guix-config
```

### Home

Copy the `guix home` configuration.

``` shell
cp guix-config/home/config.scm config.scm
```

Modify it to suit your circumstance.

``` shell
nano config.scm
```

Reconfigure your home.

``` shell
guix home reconfigure config.scm
```

### System

Copy the `guix system` configuration.

``` shell
cp guix-config/system/config.scm /etc/config.scm
```

Modify it to suit your circumstance.

``` shell
nano /etc/config.scm
```

Reconfigure your system.

``` shell
guix system reconfigure /etc/config.scm
```

## License

GNU General Public License v3.0 or later. See `COPYING`.

(use-modules
 (gnu)
 (gnu home)
 (gnu home services)
 (gnu home services desktop)
 (gnu home services guix)
 (gnu home services shells)
 (gnu home services shepherd)
 (gnu packages)
 (gnu packages android)
 (gnu packages bittorrent)
 (gnu packages browser-extensions)
 (gnu packages chromium)
 (gnu packages cvassistant)
 (gnu packages disk)
 (gnu packages ebook)
 (gnu packages emacs)
 (gnu packages finance)
 (gnu packages flashing-tools)
 (gnu packages freedesktop)
 (gnu packages gnucash)
 (gnu packages gnupg)
 (gnu packages gnuzilla)
 (gnu packages irc)
 (gnu packages kerberos)
 (gnu packages libreoffice)
 (gnu packages linphone)
 (gnu packages lxde)
 (gnu packages lxqt)
 (gnu packages mail)
 (gnu packages messaging)
 (gnu packages networking)
 (gnu packages pdf)
 (gnu packages polkit)
 (gnu packages pulseaudio)
 (gnu packages ssh)
 (gnu packages telegram)
 (gnu packages telephony)
 (gnu packages version-control)
 (gnu packages video)
 (gnu packages web-browsers)
 (gnu services)
 (guix channels)
 (guix gexp))

(home-environment
 (packages
  (append
   ;; Communication
   (list
    claws-mail
    gajim
    gajim-omemo
    gajim-openpgp
    icedove
    linphone-desktop
    mumble
    mutt
    poezio
    telegram-desktop
    weechat)
   ;; DevOps
   (list
    adb
    fastboot
    git
    `(,git "send-email")
    heimdall
    openssh)
   ;; Multimedia
   (list
    mpv
    pipe-viewer
    vlc
    yt-dlp)
   ;; Network
   (list
    `(,transmission "gui"))
   ;; Productivity
   (list
    calibre
    cvassistant
    emacs
    gnucash
    homebank
    libreoffice)
   ;; Security
   (list
    gnupg
    gpa
    lxqt-policykit
    pinentry)
   ;; Utilities
   (list
    blueman
    mupdf
    pavucontrol
    udiskie
    xfe)
   ;; Web
   (list
    icecat
    nyxt
    ublock-origin/chromium
    uget
    ungoogled-chromium)))
 (services
  (append
   ;; Shells
   (list
    (service home-bash-service-type
	     (home-bash-configuration
	      (bash-profile
	       (list
		(local-file
		 (string-append (getenv "HOME")
				"/dotfiles/bash/bash_profile")
		 "bash_profile"))))))
   ;; Channels
   (list
    (simple-service 'guix-channels
                    home-channels-service-type
                    (list
                     #;(channel
                      (name 'guixrus)
                      (url "https://git.sr.ht/~whereiseveryone/guixrus")
                      (introduction
                       (make-channel-introduction
                        "7c67c3a9f299517bfc4ce8235628657898dd26b2"
                       (openpgp-fingerprint
                        "CD2D 5EAA A98C CB37 DA91  D6B0 5F58 1664 7F8B E551"))))
                     (channel
                      (name 'nonguix)
                      (url "https://gitlab.com/nonguix/nonguix")
                      (introduction
                       (make-channel-introduction
                        "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
                       (openpgp-fingerprint
                        "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
                     (channel
                      (name 'rg)
                      (url "https://git.sr.ht/~raghavgururajan/guix-channel")
                      (introduction
                       (make-channel-introduction
                        "b56a4dabe12bfb1eed80467f48d389b32137cb60"
                       (openpgp-fingerprint
                        "CD2D 5EAA A98C CB37 DA91  D6B0 5F58 1664 7F8B E551")))))))
   ;; Files
   (list
    (simple-service 'emacs-config
		    home-files-service-type
		    (list
		     `(".emacs.d/init.el"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/emacs/init.el")))))
    (simple-service 'git-config
		    home-files-service-type
		    (list
		     `(".config/git/config"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/git/config")))))
    (simple-service 'gnupg-dirmngr-config
		    home-files-service-type
		    (list
		     `(".gnupg/dirmngr.conf"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/gnupg/dirmngr.conf")))))
    (simple-service 'gnupg-gpa-config
		    home-files-service-type
		    (list
		     `(".gnupg/gpa.conf"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/gnupg/gpa.conf")))))
    (simple-service 'gnupg-gpg-config
		    home-files-service-type
		    (list
		     `(".gnupg/gpg.conf"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/gnupg/gpg.conf")))))
    (simple-service 'gnupg-gpg-agent-config
		    home-files-service-type
		    (list
		     `(".gnupg/gpg-agent.conf"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/gnupg/gpg-agent.conf")))))
    (simple-service 'gnupg-scdaemon-config
		    home-files-service-type
		    (list
		     `(".gnupg/scdaemon.conf"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/gnupg/scdaemon.conf")))))
    (simple-service 'gnupg-sshcontrol-config
		    home-files-service-type
		    (list
		     `(".gnupg/sshcontrol"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/gnupg/sshcontrol")))))
    (simple-service 'gtk-gtk2-config
		    home-files-service-type
		    (list
		     `(".gtkrc-2.0"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/gtk/gtkrc-2.0")))))
    (simple-service 'gtk-gtk3-config
		    home-files-service-type
		    (list
		     `(".config/gtk-3.0/settings.ini"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/gtk/settings.ini")))))
    (simple-service 'nano-config
		    home-files-service-type
		    (list
		     `(".config/nano/nanorc"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/nano/nanorc")))))
    (simple-service 'poezio-config
		    home-files-service-type
		    (list
		     `(".config/poezio/poezio.cfg"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/poezio/poezio.cfg")))))
    (simple-service 'poezio-irc-config
		    home-files-service-type
		    (list
		     `(".config/poezio/plugins/irc.cfg"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/poezio/plugins/irc.cfg")))))
    (simple-service 'poezio-link-config
		    home-files-service-type
		    (list
		     `(".config/poezio/plugins/link.cfg"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/poezio/plugins/link.cfg")))))
    (simple-service 'poezio-otr-config
		    home-files-service-type
		    (list
		     `(".config/poezio/plugins/otr.cfg"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/poezio/plugins/otr.cfg")))))
    (simple-service 'stumpwm-config
		    home-files-service-type
		    (list
		     `(".stumpwm.d/init.lisp"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/stumpwm/init.lisp")))))
    (simple-service 'termite-config
		    home-files-service-type
		    (list
		     `(".config/termite/config"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/termite/config")))))
    (simple-service 'x-xinit-config
		    home-files-service-type
		    (list
		     `(".xinitrc"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/x/xinitrc")))))
    (simple-service 'x-xserver-config
		    home-files-service-type
		    (list
		     `(".xserverrc"
		       ,(local-file
			 (string-append (getenv "HOME")
					"/dotfiles/x/xserverrc"))))))
   ;; Daemons
   (list
    (service home-redshift-service-type)
    (service home-shepherd-service-type
	     (home-shepherd-configuration
	      (services
	       (list
		(shepherd-service
		 (provision '(adb))
		 (start #~(make-system-constructor "adb start-server"))
		 (stop #~(make-system-destructor "adb kill-server")))
		(shepherd-service
		 (provision '(emacs))
		 (start #~(make-system-constructor "emacs --daemon"))
		 (stop #~(make-system-destructor "emacsclient -e '(kill-emacs)'")))
		(shepherd-service
		 (provision '(gpg-agent))
		 (start #~(make-system-constructor "gpg-agent --daemon"))
		 (stop #~(make-system-destructor "gpg-connect-agent killagent /bye")))
		(shepherd-service
		 (provision '(polkit-agent))
		 (start #~(make-system-constructor "lxqt-policykit-agent &"))
		 (stop #~(make-system-destructor "pkill -9 lxqt-policykit-agent")))
		(shepherd-service
		 (provision '(udiskie))
		 (start #~(make-system-constructor "udiskie &"))
		 (stop #~(make-system-destructor "pkill -9 udiskie")))))))))))

